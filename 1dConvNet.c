#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int band_size = 4, inputFrame = 3;  // column ~ InputFrame
int kenal_width = 1;
int outputFrame = 3;

void onedimensionlayerisfun(int matrixB[band_size][inputFrame], // matrixA[7][3]
    int weightB[outputFrame][outputFrame*kenal_width], //weightA[1][3] //output[7][1]
    int biasB[outputFrame],
    int outputB[band_size-kenal_width+1][outputFrame]){

    printf("\n---Test begin----\n\n Output Matrix: \n");
    int output_height = band_size-kenal_width+1;
    int output_width = outputFrame;
    for(int i=0; i< output_height; i++ ){
        for(int j =0; j<output_width; j++){
            printf("%i ", outputB[i][j]);
        }
        printf("\n");

    }


}
int main(){

    //Initalise matrix
    int matrixA[band_size][inputFrame]; // matrixA[7][3]
    int weightA[outputFrame][outputFrame*kenal_width]; //weightA[1][3]
    int output[band_size-kenal_width+1][outputFrame]; //output[7][1]
    int biasA[outputFrame];

    // Output matrix = [1,2,3],[3,4,5]...[19,20,21]  with shape 7x3
    int *element;
    element = (int *)malloc(sizeof(int));
    *element = 1;

    printf("\nInput Matrics:\n");
    for(int i=0; i<band_size; i++){
        for(int j=0; j<inputFrame ; j++){
            matrixA[i][j] = *element;
            printf(" %d ", *element);
            ++(*element);
        }
        printf("\n");
    }
    free(element);


    //Ininitalize Weight element
    int intermediate_dimension = inputFrame * kenal_width;
    printf("network weights:\n");
    for(int i=0; i<outputFrame; i++){
        for(int j=0; j < intermediate_dimension; j++){
            weightA[i][j] = 10+i+j;
            printf(" %i ", weightA[i][j]);
        }
        printf("\n");
    }

    //Initalize biases
    printf("\nBiases: \n");
    for(int i=0; i<outputFrame; i++){
        biasA[i] = i*8 + 3+ 1;
        printf(" %i \n", biasA[i]);
    }
    printf("\n");
    

    //Implement 1d convNet in Torch style on C
    int testVal = 0;
    //Output image height = originalSize - kenal width + kenal height(1) 
    int out_height = band_size-kenal_width+1; // 22-3+1 or 7-1+1

    for(int i=0 ;i < out_height; i++ ){    // Output row
        for(int j=0; j< outputFrame; j++){   //Out put Column

            int counter = 0; //Intermediate weight
            int dot_process = 0; //Intermediate dot product calculation
            for(int k_w = 0; k_w < kenal_width; k_w++ ){
                for(int k_len = 0; k_len< inputFrame; k_len++){
                    dot_process += matrixA[i+k_w][k_len] * weightA[j][counter];
                    counter++;
                }
            }

            dot_process = dot_process + biasA[j] ;//biasA[j]
          //  printf("dot_process at [%i][%i]: %i \n" ,i,j, dot_process);
            output[i][j] = dot_process;
            ++testVal;
        }
    }

    //Debug
    printf("Random access element: %i", output[1][2]);
    printf("\n Test value, number of element :%i\n", testVal);

     onedimensionlayerisfun(matrixA,weightA,biasA,output);
return 0;
}
